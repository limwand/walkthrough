<?php
error_reporting(-1);
ini_set('display_errors', 'On');
ini_set('memory_limit', '512M');

function distance($lat1, $lon1, $lat2, $lon2) {
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;

	return ($miles * 1.609344);
}

function getFiles($dir = 'tmp/'){
	$files = array();
	if ($handle = opendir($dir)) {
	    while (false !== ($entry = readdir($handle))) {
	        if ($entry != "." && $entry != "..") {
	           	$files[] = $entry;
	        }
	    }
	    closedir($handle);
	}

	return $files;
}

function determineNearestBusstop($latitude, $longitude, $num = 3){

	$text = file_get_contents('stopareacode.json');
	$json = json_decode($text);

	$minDistance = array();
	$minBusstops = array();
	foreach($json as $obj)
	{
		$newDistance = distance($latitude, $longitude, (float)$obj->Latitude, (float)$obj->Longitude);

		for($i = 0; $i < $num; $i++){ 
			if(!isset($minDistance[$i]) || $newDistance <= $minDistance[$i]) {
				$minDistance[$i] = $newDistance;
				$minBusstops[$i] = $obj;
				break;
			}
		}
	}

	return $minBusstops;

}

function determineStop($stopFrom, $stopTo) {

	if($stopFrom != false && $stopTo != false)
		return;

	// Create a stream
	$opts = array(
	  'http'=>array(
	    'method'=>"GET",
	    'header'=>"Accept-Language:en-US,en;q=0.8,nl;q=0.6\r\n" .
	              "User-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36	\r\n"
	  )
	);

	$tpcs = array();
	foreach($stopFrom as $stop) {
		$tpcs[] = $stop->TimingPointCode;
	}

	if(count($tpcs) == 0)
		return;

	$context = stream_context_create($opts);
	$file = file_get_contents('http://v0.ovapi.nl/tpc/' . implode(',', $tpcs) . '/departures', false, $context);

	if($file) {
		file_put_contents('tmp/' . time() . '.json', $file);
	}
	else
	{
		$files = getFiles();
		$pos = array_rand($files);
		$file = file_get_contents('tmp/' . $files[$pos]);
	}

	return json_decode($file);
}

if(isset($_GET['from_lat']))
	$fromLat = (float)$_GET['from_lat'];
else
	$fromLat = 52.0848330;

if(isset($_GET['from_lng']))
	$fromLng = (float)$_GET['from_lng'];
else
	$fromLng = 5.1703430;

if(isset($_GET['to_lat']))
	$toLat = (float)$_GET['to_lat'];
else
	$toLat = 52.0901530;

if(isset($_GET['to_lng']))
	$toLng = (float)$_GET['to_lng'];
else
	$toLng = 5.1226020;

$stopFrom = determineNearestBusstop($fromLat, $fromLng);
$stopTo = determineNearestBusstop($toLat, $toLng);
$routes = determineStop($stopFrom, $stopTo);


$acceptedRoutes = array();

do {
	if($routes != null){
		foreach($routes as $route) {
			foreach($route->Passes as $mogelijkheid) {
				$acceptedRoutes[] = $mogelijkheid;
			}
		}
	}

	if(count($routes) == 0 || $routes == null){
		$files = getFiles();
		$pos = array_rand($files);
		$file = file_get_contents('tmp/' . $files[$pos]);
		$routes = json_decode($file);
	}
}
while(count($acceptedRoutes) == 0);

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

echo json_encode($acceptedRoutes);	