var map;
var pointsChargers = [
    {
        "lat": 52.06928,
        "lng": 5.09825,
        "available": false
    },
    {
        "lat": 52.09475,
        "lng": 5.05568,
        "available": true
    },
    {
        "lat": 52.11247,
        "lng": 5.06804,
        "available": false
    },
    {
        "lat": 52.11753,
        "lng": 5.08589,
        "available": true
    },
    {
        "lat": 52.10361,
        "lng": 5.10786,
        "available": false
    {
        "lat": 52.09138,
        "lng": 5.12778,
        "available": true
    },
    {
        "lat": 52.08421,
        "lng": 5.13258,
        "available": true
    },
    {
        "lat": 52.11331,
        "lng": 5.0955,
        "available": true
    },
    {
        "lat": 52.07366,
        "lng": 5.10992,
        "available": false
    },
    {
        "lat": 52.06184,
        "lng": 5.11954,
        "available": true
    },
    {
        "lat": 52.07999,
        "lng": 5.03439,
        "available": true
    },
    {
        "lat": 52.11452,
        "lng": 5.09851,
        "available": false
    },
    {
        "lat": 52.10778,
        "lng": 5.12872,
        "available": false
    },
    {
        "lat": 52.09449,
        "lng": 5.12975,
        "available": false
    },
    {
        "lat": 52.10187,
        "lng": 5.09611,
        "available": true
    },
    {
        "lat": 52.12591,
        "lng": 5.05834,
        "available": true
    },
    {
        "lat": 52.10335,
        "lng": 5.04529,
        "available": false
    },
    {
        "lat": 52.07698,
        "lng": 5.05216,
        "available": false
    },
    {
        "lat": 52.06727,
        "lng": 5.10572,
        "available": false
    },
    {
        "lat": 52.05355,
        "lng": 5.11018,
        "available": true
    }
];
var nonce = 0;
var responses = [];

var method_translation = {
    "cycling": "Cycling",
    "walking": "Walking",
    "driving": "Driving",
    "public": "Public Transportation",
    "prius": "Electric car"
};

var onSuccess = function(position) {
 
    Longitude   = position.coords.longitude;
    Latitude    = position.coords.latitude;
    /* alert('Latitude: '          + position.coords.latitude          + '\n' +
      'Longitude: '         + position.coords.longitude         + '\n' +
      'Altitude: '          + position.coords.altitude          + '\n' +
      'Accuracy: '          + position.coords.accuracy          + '\n' +
      'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
      'Heading: '           + position.coords.heading           + '\n' +
      'Speed: '             + position.coords.speed             + '\n' +
      'Timestamp: '         + position.timestamp                + '\n');*/
    lonLat(Longitude,Latitude);
};

var updateGeo = function(pos) {
    //setTimeout(function() {
    //    navigator.geolocation.getCurrentPosition(updatGeo);
    //});
}

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}
                
function loadMap() {
    var app = {

        // Application Constructor
        initialize: function () {
            this.bindEvents();
        },

        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function () {
            document.addEventListener('deviceready', this.onDeviceReady, false);
        },

        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicity call 'app.receivedEvent(...);'
        onDeviceReady: function () {
            app.receivedEvent('deviceready');
        },

        // Update DOM on a Received Event
        receivedEvent: function (id) {

            console.log('Cordova device ready event: ' + id);

            var info_symbol = new esri.symbol.PictureMarkerSymbol({
                "angle": 0,
                "xoffset": 2,
                "yoffset": 8,
                "type": "esriPMS",
                "url": "./images/green-pin.png",
                "contentType": "image/png",
                "width": 24,
                "height": 24
            });

            var info_spark = new esri.symbol.PictureMarkerSymbol({
                "angle": 0,
                "type": "esriPMS",
                "url": "images/spark.png",
                "contentType": "image/png",
                "width": 18,
                "height": 21
            });

            var info_spark_red = new esri.symbol.PictureMarkerSymbol({
                "angle": 0,
                "type": "esriPMS"   ,
                "url": "images/sparkred.png",
                "contentType": "image/png",
                "width": 18,
                "height": 21
            });

            var info_symbol2 = new esri.symbol.PictureMarkerSymbol({
                "angle": 0,
                "xoffset": 2,
                "yoffset": 8,
                "type": "esriPMS",
                "url": "http://static.arcgis.com/images/Symbols/Basic/RedShinyPin.png",
                "contentType": "image/png",
                "width": 24,
                "height": 24
            });

            require([
                    "esri/map", 
                    "esri/graphic", 
                    "esri/geometry/Point", 
                    "esri/geometry/Polyline", 
                    "esri/symbols/SimpleLineSymbol",
                    "esri/symbols/SimpleMarkerSymbol",
                    "esri/Color",
                    "dojo/dom", 
                    "dojo/dom-attr",
                    "esri/layers/GraphicsLayer", 
                    "dojo/domReady!"],
                function (Map, Graphic, Point, Polyline, SimpleLineSymbol, SimpleMarkerSymbol, Color, dom, domAttr, GraphicsLayer) {
                    // Create map
                    map = new Map("mapDiv", {
                        basemap: "satellite", // old: satellite
                        //basemap: "streets", // old: satellite
                        center: [Longitude, Latitude],
                        zoom: 15
                    });

                    this.updateGeo = function(pos) {
                     //   var lon = pos.coords.longitude;
                      //  var lat = pos.coords.latitude;

                      //  var pt = new Point(lon, lat);
                      //  map.centerAt(pt);
                    }

                    function orientationChanged() {
                        if(map){
                            map.reposition();
                            map.resize();
                        }
                    }

                        //if (navigator.geolocation) {
                    //
                    //    jQuery("#btnInit").click();
                    //    initiate_geolocation
                    //
                    //    function initiate_geolocation() {
                    //        navigator.geolocation.getCurrentPosition(handle_geolocation_query);
                    //    }
                    //
                    //    function handle_geolocation_query(position) {
                    //        alert('Lat: ' + position.coords.latitude + ' ' + 'Lon: ' + position.coords.latitude);
                    //    }
                    //
                    //} else {
                    //    alert("I'm sorry, but geolocation services are not supported by your browser.");
                    //}


                    //navigator.geolocation.getCurrentPosition(function(pos) {
                    //        console.log(pos);
                    //    },
                    //    function (err) {
                    //        console.log("Error: ", err);
                    //    }, {timeout: 5000});

                    //setInterval(function() {
                    //    if(navigator.geolocation) {
                    //        navigator.geolocation.watchPosition(updateGeo, error, {enableHighAccuracy: false, maximumAge: 15000, timeout: 30000});

                        //    console.log("Setting geo");
                        //} else {
                        //    alert("Geolocation is not supportedd!");
                        //}
                    //}, 1000);

                    function error(err) {
                        console.log("Error: ", err);
                    }

                    //function updateGeo(pos) {
                    //    console.log(pos);
                    //}

                    map.on("load", function () {
                        $( "html" ).on( "click", "#search-button", function() {
                            doSearch();
                        });

                        $( "#events" ).on( "click", "i", function() {

                            var options = {
                                enableHighAccuracy: true,
                                timeout: 5000,
                                maximumAge: 0
                            };

                            var lat = $( this ).parent().attr('data-lat');
                            var lng = $( this ).parent().attr('data-lng');

                            $('.closeall').hide();
                            routeInfoDisplay();

                            if (navigator.geolocation) {
                                navigator.geolocation.getCurrentPosition(
                                    function(position) {
                                        // Get current cordinates.
                                        var from = { 'lat': position.coords.latitude, 'lon': position.coords.longitude };
                                        var to = { 'lat': lng, 'lon': lat } ;

                                        addRoutePoints(from, to);
                                        chooseWayToTravel( from, to );
                                    },
                                    function(error) {
                                        // KRAK
                                        var from = { 'lon': 52.085395, 'lat': 5.169086 };
                                        var to = { 'lat': lng, 'lon': lat  };

                                        console.log(error);
                                        addRoutePoints(from, to);
                                        chooseWayToTravel( from, to );
                                    },
                                    {timeout: 2000, enableHighAccuracy: true, maximumAge: 3000}
                                );
                            }
                        });

                        $(".osm_search_field").keypress(function(e) {
                           if(e.keyCode == 13) {
                               doSearch();
                           }
                        });

                        var gl = new GraphicsLayer();

                        $.each( pointsChargers, function( i, charger ) {

                            console.log(charger);

                            var p = new Point(charger.lng,charger.lat);

                            if(charger.available){
                                info_spark.charger = charger;
                                var g = new Graphic(p, info_spark);
                            }
                            else{
                                info_spark_red.charger = charger;
                                var g = new Graphic(p, info_spark_red);
                            }

                            gl.add(g);                        
                        });

                        map.addLayer(gl);

                        dojo.connect(gl, "onClick", function(evt){
                            map.infoWindow.setTitle("Power charging station");

                            if(evt.graphic.symbol.charger.available == true){
                                map.infoWindow.setContent("Available");
                            }
                            else
                            {
                                map.infoWindow.setContent("Unavailable, still charging.");
                            }
                            map.infoWindow.show(evt.screenPoint,map.getInfoWindowAnchor(evt.screenPoint));
                        });

                      
                        // TODO: Implement some dropdown functionality
                        //$("#osm_search_field").keydown(function(e) {
                        //    console.log(e.keyCode)
                        //    var text = e.target.value;
                        //    clearTimeout(window.searchTimeout);
                        //    $("#search_result").remove();
                        //
                        //    if(text.length > 2) {
                        //        window.searchTimeout = setTimeout(function () {
                        //            doSearch($('#osm_search_field').val());
                        //        }, 100)
                        //    }
                        //});
                        //
                        //$("#osm_search_field").keydown(function(e) {
                        //   if(e.keyCode == 8) {
                        //       $("#search_result").remove();
                        //   }
                        //});

                        //console.log(Longitude, Latitude);

                        // Get the textfields with the class: osm
                        //$.getJSON("http://nominatim.openstreetmap.org/search/Utrecht?format=json&addressdetails=1&dedupe=1", function(data) {
                            //console.log(data);
                        //});
                    });

                    function addRoutePoints(from, to) {
                        var p1 = new Point(from.lat, from.lon);
                        var p2 = new Point(to.lat, to.lon);

                        //console.log(p1, p2);

                        map.graphics.add(new Graphic(p1, info_symbol));
                        map.graphics.add(new Graphic(p2, info_symbol2));
                    }   


                    function doSearch() {
                        $('.closeall').hide();
                        $('#search').show();

                        if($("#search_to").val().length === 0) {
                            alert("You're not going anywhere?");
                        } else {
                            var from = $("#search_from").val();
                            var to = $("#search_to").val();

                            // If 'from' is empty, get the latitude and longitude from the current location
                            var from_long_lat;

                            if(from.length === 0) {
                                from_long_lat = {lat:Latitude, lon:Longitude};
                            } else {
                                from_long_lat = getLongAndAlt(from);
                            }

                            var to_long_lat = getLongAndAlt(to);

                            // TODO: Better error checking
                            if(from_long_lat === undefined || to_long_lat === undefined) {
                                alert("Something went wrong, From: " + (from_long_lat === undefined)+ ", to: " +(to_long_lat === undefined));
                            } else {

                                var from = {lat: from_long_lat.lon, lon: from_long_lat.lat};
                                var to = {lat: to_long_lat.lon, lon: to_long_lat.lat};

                                addRoutePoints(from, to);
                                // Hide the input
                                chooseWayToTravel(from, to);

                            }

                        }
                    }

                    function chooseWayToTravel(from, to) {

                        routeInfoDisplay();
                        map.graphics.clear();

                        $("#search-button").attr("disabled", "disabled");
                        routeInfoDisplay();

                        routeInfo(from, to, function(response) {
                            //createDirections(from, to, response[0].route);
                            //$(".closeall .input").remove();   

                            if(response.text == null)
                            {
                                response.text = 'takes ' + Math.round(response.time) + ' minutes ';

                                if(response.method == 'walking' || response.method == 'cycling'){
                                    if(response.rain > 0){
                                        response.text += ', <b>expect rain</b>!';
                                    }
                                    else
                                    {
                                         response.text += ', <b>no rain</b> is to be expected, nice and sunny!';   
                                    }
                                }
                            }
                            else
                            {
                                response.text = 'takes ' + Math.round(response.time) + ' minutes ' + response.text ;
                            }

                            responses[nonce] = response;

                            $("#route-info").append("<div route=\"" + nonce++ + "\" class=\"" + response.method + "\" style=\"clear: both;\"><h2 style=\"float: left;\">&nbsp;" +  response.method.charAt(0).toUpperCase() + response.method.slice(1) + "</h2><br /><p style=\"float: left; color: white;\">" + response.text + "<br />" + response.motivation + "<p></div>");
                           
                            $("#route-info div").click(function(e) {
                                var route_number = parseInt($(this).attr("route")) ;

                                $("#route-info").html("<div id='nav-info'><p>Turn right in 200 meters</p><span>Then follow the Padualaan</span><div class=\"load\"><br /><br /><img src=\"images/ajax-loader.gif\" style=\"margin-bottom: -6px;\" /> Loading the route</div></div>");
                                $("#route-info").append("<img id='nav-right' class='nav-icon' src='./images/icons/directions/right.png'/>");
                                    
                                $("#route-info").show(); // TEMP'

                                createDirections(from, to, responses[route_number].route);
                            });
                        });
                    }

                    function createDirections(from, to, route) {
                        var symbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new dojo.Color([98, 211, 125]), 10);

                        for(var i = 0; i < route.length - 1; i ++) {
                            var point1 = new Point(route[i][0], route[i][1]);
                            var point2 = new Point(route[i + 1][0], route[i + 1][1]);

                            var polyline = new Polyline();
                            polyline.addPath([point1, point2]);

                            map.graphics.add(new Graphic(polyline, symbol));
                            $('.load').hide();
                        }
                    }
                });
        }
    };

    function getJSON(url) {
        var ret = undefined;

        $.ajax({
            url: url,
            dataType: "json",
            success: function(e) {
                ret = e;
            },
            async: false
        });

        return ret;
    }

    function getLongAndAlt(text) {
        var new_text = text.replace(" ", "+");
        var json = getJSON("http://nominatim.openstreetmap.org/search.php?q="+new_text+"&format=json&addressdetails=1&dedupe=1");

        if(json === undefined) return undefined;

        var final_obj = {
            lat: Number(json[0]['lat']),
            lon: Number(json[0]['lon'])
        }

        return final_obj;
    }

    //function doSearch(keywords) {
        //$.getJSON("http://nominatim.openstreetmap.org/search/" + keywords + "?format=json&addressdetails=1&dedupe=1", function(data) {
        //    for(var i = 0; i < data.length; i ++) {
        //        var html = $.parseHTML("<p id='search_result'>"+data[i].display_name+"</p>");
        //        $("#search").append(html);
        //    }
        //});
    //}

    return app.initialize();
}

//loving it
var clientID = 'x92MBkmMeP0kRirS';
var secret = 'f9e13b7c6efc4bf988340e1779b45fa1';
var ARCGISTOKEN = '1Z3IL941tfTvEsGe_VpTkbTAGb6QJoVJ4P8c8vD7x-UEPYZV3AXCsQrUjh0SzeXZeqLd6N0jGTP4vza6_jBiflWVHfddpsTcZSHQmlI1YxYAjsbRuyDoHUMocBZjoDXhkTH4pb3prf-KVpzaR5pXGg..';
var CYCLEMULTIPLE = 4;
var travelModes = {
  walking : '6e21315077644062a34e34f3be2e4c86',
  driving : 'd14d65f191744ddc88e331b239d4fd9f',
}

$(document).ready(function() {
    getToken();
})

function getToken(callback) {
  $.getJSON('http://albron.philip-skinner.co.uk:81/token', function(response) {
    if (response.access_token) {
      ARCGISTOKEN = response.access_token;

      //better check it exists
      if (callback) {      
        callback();
      }
    }
  });
}

function routeInfo(from, to, callback) {
	if (ARCGISTOKEN == null) {
		alert("Token is null");
	}

	var obj = { callback : callback,  results : [] };

	getRouteInfo(travelModes.walking, from, to, obj, 'walking');
	getRouteInfo(travelModes.walking, from, to, obj, 'cycling');
	getRouteInfo(travelModes.driving, from, to, obj, 'driving');
	getRouteInfo(travelModes.driving, from, to, obj, 'prius');
	
    // The callbakc is fucked up. 
    getBusRouteInfo('Public transport', from, to, obj, 'public');
}

var caloriesPerMinute = 9;
function _routeInfo(response, obj) {

    if(response.method == 'prius') {
        response.motivation = '<span style="color: orange;">Reduces ' + Math.round(57.75*response.time) + 'g CO2 emission</span>';
    }
    if(response.method == 'walking') {
        response.motivation = '<span style="color: green;">Burned ' + Math.round(caloriesPerMinute*response.time) + ' calories</span>'
                            + ' <span style="color: green;">' + Math.round((response.time/60)*100) + '% daily exercise</span>';
    }
    if(response.method == 'driving') {
        response.motivation = '<span style="color: red;">Cost &euro;' + (0.787*response.time).toFixed(2) + ' gasoline</span>';
    }
    if(response.method == 'cycling') {
        response.motivation = '<span style="color: green;">Burned ' + Math.round(caloriesPerMinute*response.time) + ' calories</span>'
                            + ' <span style="color: green;">Reduces ' + Math.round(105*response.time) + 'g CO2 emission</span>';
    }
    if(response.method == 'Public transport') {
        response.motivation = '<span style="color: green;">Burned ' + Math.round(caloriesPerMinute*response.time) + ' calories</span>';
    }

	$('.load').hide();
	obj.callback(response);
}

function _rainInfo(raining) {
    if (raining) {
      this.ret.rain = true;
    }
    this.ret.num--;
    
    if (this.ret.num == 0) {
      this.callback(this.ret, this.obj);
    }
}

function deg2rad(angle) {
  return angle * .017453292519943295; // (angle / 180) * Math.PI;
}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}


function getRouteInfo(method, from, to, obj, name, checkrain) {
	var text = null;

	if(name == 'prius') {

		var minDist = 100000;
		var closePower = null;
		for(var i = 0; i < pointsChargers.length; i++ )
		{
			var dist = getDistanceFromLatLonInKm(pointsChargers[i].lat, pointsChargers[i].lng, to.lon,to.lat);

			if(dist < minDist && pointsChargers[i].available) {
				closePower = pointsChargers[i];
				minDist = dist;
			}
		}	

		if(minDist <= 1) {
			to.lat = closePower.lng;
			to.lon = closePower.lat;
			text = " and a walk of " + Math.round( minDist * 14 ) + " minutes after parking the prius at the power station.";
		}
		else {
			return;
		}
	}

    var stops = 'stops=' + from.lat + ',' + from.lon + ';' + to.lat + ',' + to.lon;

	$.getJSON("http://route.arcgis.com/arcgis/rest/services/World/Route/NAServer/Route_World/solve?token=" + ARCGISTOKEN + "&" + stops + "&f=json&travelMode=" + method, function(response) {
        var ret = {
			'method' : name,
			'time' : 0,
			'rain' : false,
			'num' : 0,
			'text' : text,
			'route' : [],
		};

        if (response && response.routes && response.routes.features && response.routes.features.length > 0) {
          var route = response.routes.features[0];
          if (route.geometry && route.geometry.paths && route.geometry.paths.length > 0) {
            ret.route = route.geometry.paths[0];
          }
        }

		if (response && response.directions && response.directions.length > 0) {
			var directions = response.directions[0];
			if (directions.summary) {
				if (directions.summary.totalTime) {
					ret.time += directions.summary.totalTime;
					
					//dirty
					if (name == 'cycling') {
					  ret.time /= CYCLEMULTIPLE;
					}
				}
			}
            if (directions.features && directions.features.length > 0) {
                //we have some points
                ret.num = directions.features.length;
                for (var i = 0; i < directions.features.length; i++) {
                    var latlng = _fromCompressedGeometry(directions.features[i].compressedGeometry)[0];
                
                    willRain(latlng[0], latlng[1], directions.features[i].attributes.time, _rainInfo.bind({ ret : ret, callback : _routeInfo, obj : this }));
                }
             }    
		}
	}.bind(obj));
}

function willRain(lat, lon, timeTo, callback) {
	//will it rain?
	//its holland, so yes.
		
	$.getJSON('http://albron.philip-skinner.co.uk:81/proxy/' + lat + '/' + lon, function(response) {
	//	var now = Date.UTC()/1000;
	//	var variation = 0.1;
	//	var lowerBound = now - Math.ceil(now * variation);
	//	var upperBound = now + Math.ceil(now * variation);
		var rain = 0;

        if (response && response.times && response.times.length > 0) {
			for (var i = 0; i < response.times.length; i++) {

				rain += response.times[i].rain; // Even duidelijk maken voor de presentatie
				/*
				if (response.times[i].rain > 0) {                  
					var parts = response.times[i].time.split(':');
					var seconds = ((parseInt(parts[0]) * 60) + parseInt(parts[1])) * 60;

					if (seconds >= lowerBound && seconds <= upperBound) {
						rain = true; 
					}
				}*/
			}
        }

        this.callback(rain);
	}.bind({ callback : callback }));
}

//stole this from a github repo
function _fromCompressedGeometry(str) {
  var xDiffPrev = 0,
    yDiffPrev = 0,
    points = [],
    x, y,
    strings,
    coefficient;

  // Split the string into an array on the + and - characters
  strings = str.match(/((\+|\-)[^\+\-]+)/g);

  // The first value is the coefficient in base 32
  coefficient = parseInt(strings[0], 32);

  for (var j = 1; j < strings.length; j += 2) {
    // j is the offset for the x value
    // Convert the value from base 32 and add the previous x value
    x = (parseInt(strings[j], 32) + xDiffPrev);
    xDiffPrev = x;

    // j+1 is the offset for the y value
    // Convert the value from base 32 and add the previous y value
    y = (parseInt(strings[j + 1], 32) + yDiffPrev);
    yDiffPrev = y;

    points.push([x / coefficient, y / coefficient]);
  }

  return points;
}
//unction getRouteInfo(method, from, to, obj, name)

function getBusRouteInfo(method, from, to, obj, name) {
    $.ajax({
        type: 'GET',
        url: 'http://www.run2.be/api.php?from_lat=' + from.lat + '&from_lng' + from.lon + '&to_lat=' + to.lat + '&to_lng' + to.lon,
        crossDomain: true,
        success: function(response) {

    		response.text = " to walk to the bus, then bus to " + response.DestinationName50 + " departs at " + response.TargetDepartureTime + " and takes " +  Math.round( response.busTime ) + " minutes.";

			response.method = method;
			response.name = name;

            getRouteInfo(travelModes.walking, from, { lat:response.Longitude, lon:response.Latitude }, { callback : _busCallback.bind({ callback : _routeInfo, response : response, obj : this }), results : [] }, 'walking', true);
        }.bind(obj)
    })
}


function _busCallback(response, obj) {
    response.method = this.response.method;
    response.text = this.response.text;

    this.callback(response, this.obj);
}
