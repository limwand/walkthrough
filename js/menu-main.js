/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    function loadmenu(){
        //alert('test');
        var menu = $("#menu-main").clone();
        $("#menu-main").remove();
        $(menu).insertAfter("#mapDiv");
       // var docwidth = ($(document).width()/100)*100;
        var docwidth = $(document).width();
        
        
        var menrad = (docwidth/100)*10;
        var menwidthheight = (menrad*2)-6;
        var menleft = (docwidth-menwidthheight)/2;
        //alert(docwidth);css('border-radius', docwidth+'px');
        $("#menu-main").css({'border-radius': menrad+'px','height': menwidthheight+'px','width': menwidthheight+'px','left': menleft+'px'});
        $("#menu-main").click(function(){showIcons();});
        $("#menu-main").append("<img id='map-pointer' class='menu-icon' src='./images/icons/menu/map-pointer.png'/>");
        $("#menu-main").append("<img id='map-routes' class='menu-icon' src='./images/icons/menu/plan-route.png'/>");
        $("#menu-main").append("<img id='map-facilities' class='menu-icon' src='./images/icons/menu/facilities.png'/>");
        //$("#menu-main").append("<img id='map-facilities' class='menu-icon' src='./images/icons/menu/facilities.png'/>");
        $("#menu-main").append("<img id='map-health' class='menu-icon' src='./images/icons/menu/health.png'/>");
        $("#menu-main").append("<img id='map-health2' class='menu-icon' src='./images/icons/menu/health.png'/>");
        
        
        var iconwidth = (docwidth/100)*10;
        var iconleft = ((menwidthheight-iconwidth)/2)+3;
        var iconbottom = menwidthheight+20;
        $(".menu-icon").width(iconwidth);
        //$(".map-pointer").width(iconwidth);
        
        $("#map-pointer").css({'width': iconwidth+'px','left': iconleft+'px', 'bottom': iconbottom+'px'});
        $("#map-routes").css({'width': iconwidth+'px','left': menwidthheight+20+'px', 'bottom': iconleft+'px'});
        $("#map-facilities").css({'width': iconwidth+'px','right': menwidthheight+20+'px', 'bottom': iconleft+'px'});
        $("#map-health2").css({'width': iconwidth+'px','left': menwidthheight+'px', 'bottom': iconleft+iconwidth+10+'px'});
        $("#map-health").css({'width': iconwidth+'px','right': menwidthheight+'px', 'bottom': iconleft+iconwidth+10+'px'});
        
        function showIcons(){
            //alert('test');
            $(".menu-icon").toggle("fast");
            /*
            if (showicons == true){
                $(".menu-icon").show();
                showicons = false;
            } else {
                $(".menu-icon").hide();
                showicons = true;
            } 
            */
        }
        
        $(".menu-icon").click(function(){
            $(".closeall").hide();
            var currentId = $(this).attr('id');
            //alert('test'+currentId);
            if(currentId == 'map-routes'){
                planRoute();
            }
            if(currentId == 'map-facilities'){
                facCats();
            }
            if(currentId == 'map-pointer'){
                $("#search").html('<h2>Plan your route:</h2>' +
                    '<label>From:</label> <br /><input type="text" name="search" class="osm_search_field" value="" id="search_from" placeholder="Current location"><br />'
                    + '<label>To:</label> <br /><input type="text" name="search" class="osm_search_field" value="Universiteit Utrecht" id="search_to"><br />'
                    + '<input type="submit"  value="Search" id="search-button">');

                $('#search').show();

              //      planRoute();  
            }
        });
    }

var fcats = false;
function facCats(){
    if (fcats == false){
        fcats = true;
    var facinfo = '<div class="closeall" id="fac-info"></div>';
        $(facinfo).insertAfter("#mapDiv");
        $("#fac-info").append("<div id='fac-infos'><h2>Find Facilities</h2></div>");
        $("#fac-info").append("<div class='facimg' id='random'><img   src='./images/icons/dart.png'/><p>Random</div>");
    } else {
        $("#fac-info").toggle("fast");
    }
    $("#random").click(function(){
        $("#events").show();
        //$(this).hide();
        $("#fac-info").hide();
    });
}

